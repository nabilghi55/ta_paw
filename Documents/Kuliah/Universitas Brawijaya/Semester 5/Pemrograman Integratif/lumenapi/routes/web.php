<?php
namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/test', function () {
    return 'This is a test route.';
});

$router->get('/user/{id}', function ($id) {
    return 'User Id = ' . $id;
});
$router->get('/post/{postId}/comments/{commentId}', function ($postId, $commentId) {
    return 'Post ID = ' . $postId . ' Comments ID = ' . $commentId;
});
$router->get('/PAW', function () {
    return 'route khusus untuk mahasiswa PAW.';
});
// $router->get('/users[/{userId}]', function ($userId = null) {
//     return $userId === null ? 'Data semua users' : 'Data user dengan id ' . $userId;
// });
$router->get('/get', function () {
    return 'GET';
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function () {
    return Str::random(32);
});

$router->post('/post', function () {
    return 'POST';
});
$router->put('/put', function () {
    return 'PUT';
});
$router->patch('/patch', function () {
    return 'PATCH';
});
$router->delete('/delete', function () {
    return 'DELETE';
});
$router->options('/options', function () {
    return 'OPTIONS';
});
$router->get('/get', function () {
    return 'GET';
});
// Route::get('/auth/login', [
//     'as' => 'route.auth.login',
//     function () {
//         return view('halo');
//     }
// ]);

// Route::get('/profile', function (Request $request) {
//     if (!$request->isLoggedIn) {
//         return redirect()->route('route.auth.login');
//     }
//     return 'Ini adalah halaman profil';
// });
// Request::macro('isLoggedIn', function () {
//     return false;
// });
